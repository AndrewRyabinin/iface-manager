TARGETS = lib iface
TEST = test_iface
ALL = $(TARGETS) $(TEST)

.PHONY: default $(ALL) clean

default: $(TARGETS)

$(TARGETS):
	@$(MAKE) -C $@;

test: $(ALL)

$(TEST):
	@$(MAKE) -C $@;

iface: lib

clean:
	$(MAKE) clean -C lib
	$(MAKE) clean -C iface
	$(MAKE) clean -C test_iface
