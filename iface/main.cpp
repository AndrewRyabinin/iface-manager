/*
 * Copyright (C)  2013 by Andrew Ryabinin
 */
#include "InterfaceManager.h"
#include "Interface.h"
#include <iostream>
#include <thread>
#include <algorithm>
#include <signal.h>


using std::shared_ptr;
using std::cout;
using std::endl;
using std::for_each;
using namespace InterfaceLib;

InterfaceManager *m;
static bool stop = false;

void printNewIface(shared_ptr<Interface>& iface_ptr) {
    cout << "NEW " << *iface_ptr << endl;
}

void printDelIface(shared_ptr<Interface>& iface_ptr) {
    cout << "GONE " << iface_ptr->name() << endl;
}

void printIface(shared_ptr<Interface>& iface_ptr) {
    cout << "IFACE " << *iface_ptr << endl;
}

void sigHandler(int i) {
    m->stopListening();
    stop = true;
}

int main() {
    struct sigaction sa = {};
    sa.sa_handler = sigHandler;
    sigaction(SIGTERM, &sa, 0);
    sigaction(SIGINT, &sa, 0);

    try {
        m = new InterfaceManager();
        m->setNewIfaceCallback(printNewIface);
        m->setDelIfaceCallback(printDelIface);
        m->startListening();

        while(!stop) {
            auto ifaces = m->interfaces();
            for_each(ifaces.begin(), ifaces.end(), printIface);
            std::this_thread::sleep_for(std::chrono::seconds(5));
        }

    }
    catch(InterfaceManagerException e) {
        cout << "ERROR: " << e.Message() << endl;
    }
    catch (...) {
        cout<< "ERROR: Unexpected exception" << endl;
    }
    delete m;
}
