/*
 * Copyright (C)  2013 by Andrew Ryabinin
 */
#include <string>

#ifndef _PARSER_H
#define _PARSER_H

class ParserException {
 public:
    ParserException(const std::string& str):_msg(str) {}
    ParserException(const char* str):_msg(str) {}
    const std::string& Message() const { return _msg; }
 private:
    std::string _msg;
};


class Parser {
public:
    Parser(const char* line):_line(line), _cur(line) {}
    void parseLine();
private:
    void parseIface();
    void parseNew();
    void parseGone();
    void next();
    void parseName();
    void parseAddr();
    void parseHexNum();
    void parseHexDigit();
    void parseColon();
    void parseType();
    const char* _line;
    const char *_cur;
};

#endif
