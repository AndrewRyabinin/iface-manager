/*
 * Copyright (C)  2013 by Andrew Ryabinin
 */
#include "Parser.h"
#include <ctype.h>

void Parser::parseLine() {
    if (*_cur == 'I') {
        parseIface();
    } else if (*_cur == 'N') {
        parseNew();
    } else if (*_cur == 'G') {
        parseGone();
    } else {
        throw ParserException("expected: IFACE, NEW or GONE");
    }
}

void Parser::parseIface() {
    if (std::string(_cur, 6) == "IFACE ") {
        next();
        parseName();
        parseAddr();
        parseType();
    } else {
        throw ParserException("expected: IFACE");
    }
}

void Parser::parseNew() {
    if (std::string(_cur, 4) == "NEW ") {
        next();
        parseName();
        parseAddr();
        parseType();
    } else {
        throw ParserException("expected: NEW");
    }
}

void Parser::parseGone() {
    if (std::string(_cur, 5) == "GONE ") {
        next();
        parseName();
    } else {
        throw ParserException("expected: GONE");
    }
}

void Parser::next() {
    while (*_cur && (*_cur) != ' ') {
        _cur++;
    }
    if (*_cur == ' ') {
        _cur++;
    }
}

void Parser::parseName() {
    next();
}

void Parser::parseAddr() {
    for (int i = 0; i < 5; i++) {
        parseHexNum();
        parseColon();
    }
    parseHexNum();
}

void Parser::parseHexNum() {
    parseHexDigit();
    if (*_cur != ':' && *_cur != ' ') {
        parseHexDigit();
    }
}

void Parser::parseHexDigit() {
    if (isxdigit(*_cur)) {
        _cur++;
    } else {
        throw ParserException("expected hex digit");
    }
}

void Parser::parseColon() {
    if (*_cur == ':') {
        _cur++;
    } else {
        throw ParserException("expected \':\'");
    }
}

void Parser::parseType() {
    while (*_cur) {
        next();
    }
}
