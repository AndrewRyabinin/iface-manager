/*
 * Copyright (C)  2013 by Andrew Ryabinin
 */
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <iostream>
#include <signal.h>
#include "Parser.h"

using std::cout;
using std::endl;
using std::cerr;

class Pipe {
public:
    explicit Pipe (const char* cmd) {
        _pipe = popen(cmd, "r");
        if (!_pipe) {
            throw "cann't execute command";
        }
    }
    ~Pipe() { pclose(_pipe); }

operator FILE*() const { return _pipe; }

private:
    FILE *_pipe;
};

Pipe *p = 0;

void sigHandler(int i) {
    delete p;
    exit(EXIT_SUCCESS);
}


void usage () {
    cout << "test_iface <path to iface executable>" << endl;
}
int main(int argc, char** argv) {
    char line[1024];
    int rc = 0;

    struct sigaction sa;
    sa.sa_handler = sigHandler;
    sigaction(SIGTERM, &sa, 0);
    sigaction(SIGINT, &sa, 0);

    try {
        if (argc != 2) {
            cerr << "ERROR: wrong arguments"<< endl;
            usage();
            return 1;
        } else {
            p = new Pipe(argv[1]);
        }

        system("ip tunnel add tun0 mode ipip remote 1.2.3.4 local 5.6.7.8");
        system("ip tunnel del tun0");
        /*
         * this test doesn't really check that NEW and GONE events happened,
         * or extra events happened, right?
         */

        while (fgets(line, sizeof(line)-1, *p) != NULL) {
            Parser parser(line);
            parser.parseLine();
        }
    } catch (ParserException e) {
        cerr << "Error: " << e.Message() << endl;
        rc = 1;
    } catch (const char * str) {
        cerr << "Error: " << str << endl;
        rc = 1;
    }
    delete p;
    return rc;
}
