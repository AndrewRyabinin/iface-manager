/*
 * Copyright (C)  2013 by Andrew Ryabinin
 */
#include <list>
#include <string>
#include <map>
#include <memory>
#include <functional>
#include <mutex>
#include <thread>
#include "Interface.h"

#ifndef _INTERFACE_MANAGER_H
#define _INTERFACE_MANAGER_H

namespace InterfaceLib {

int const MSG_BUF_SIZE = 8192;

template <typename T>
class shared_ptr_finder
{
 public:
    shared_ptr_finder(std::shared_ptr<T> const & t) : _t(t) { }

    bool operator()(std::shared_ptr<T> const & p) {
        return *p == *_t;
    }
 private:
    std::shared_ptr<T> const & _t;

};

class InterfaceManagerException {
 public:
    InterfaceManagerException(const std::string& str):_msg(str) {}
    InterfaceManagerException(const char* str):_msg(str) {}
    const std::string& Message() const { return _msg; }
 private:
    std::string _msg;
};

class InterfaceManager {
 public:
    InterfaceManager();
    ~InterfaceManager();
    std::list<std::shared_ptr<Interface>> interfaces() const;
    void setNewIfaceCallback(const std::function<void (std::shared_ptr<Interface>&)>& func);
    void setDelIfaceCallback(const std::function<void (std::shared_ptr<Interface>&)>& func);
    void listenIfaceEvents();
    void startListening();
    void stopListening();
 private:
    void fillIfaceList();
    std::list<std::shared_ptr<Interface>> _interfaces;
    std::function<void (std::shared_ptr<Interface>&)> _newIfaceCallback;
    std::function<void (std::shared_ptr<Interface>&)> _delIfaceCallback;
    int _fd; // socket descriptor
    mutable std::mutex _ifaceMutex;
    struct sockaddr_nl	_local;
    volatile bool _stopThread;
    std::thread _ifaceThread;
};

}

#endif
