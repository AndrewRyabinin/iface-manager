/*
 * Copyright (C)  2013 by Andrew Ryabinin
 */
#include "Interface.h"
#include <iostream>
#include <sstream>

namespace InterfaceLib {

std::map<int, std::string> IFACE_TYPE = {
    {1, "Ethernet"},
    {768, "IPIP tunnel"},
    {769, "IPIP6 tunnel"},
    {772, "Loopback"}, 
    {776, "Sit device - IPv6 in IPv4"}
};

Interface::Interface(struct nlmsghdr* h) {
    struct ifinfomsg *iface = reinterpret_cast<struct ifinfomsg*>(NLMSG_DATA(h));
    
    auto it = IFACE_TYPE.find(iface->ifi_type);
    if (it != IFACE_TYPE.end()) {
        _type = it->second;
    } else {
        _type = "unknown type";
    }

    int len = h->nlmsg_len - NLMSG_LENGTH(sizeof(*iface));
    for (struct rtattr *attribute = IFLA_RTA(iface); RTA_OK(attribute, len); attribute = RTA_NEXT(attribute, len)) {
        switch(attribute->rta_type) {
        case IFLA_IFNAME: 
            _name = static_cast<char *>(RTA_DATA(attribute));
            break;
        case IFLA_ADDRESS:
            {
                unsigned char* ptr = static_cast<unsigned char*>(RTA_DATA(attribute));
                std::stringstream ssaddr;
                ssaddr << std::hex
                       << (int)ptr[0] << ":"
                       << (int)ptr[1] << ":"
                       << (int)ptr[2] << ":"
                       << (int)ptr[3] << ":"
                       << (int)ptr[4] << ":"
                       << (int)ptr[5];
                _address = ssaddr.str();
                break;
            }
        default:
            break;
        }
   }

}

void Interface::print(std::ostream& os) const {
    os << _name <<" " << _address << " " << _type;
}

std::string Interface::name() const {
   return _name;
}

std::string Interface::address() const {
    return _address;
}

std::string Interface::type() const {
    return _type;
}

bool Interface::operator== (const Interface& i) const {
    return i.name() == this->name();
}

bool Interface::operator!= (const Interface& i) const {
    return !((*this) == i) ;
}

std::ostream& operator<<(std::ostream& os, const Interface& iface)
{
    iface.print(os);
    return os;
}

}
