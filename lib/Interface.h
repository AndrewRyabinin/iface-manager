/*
 * Copyright (C)  2013 by Andrew Ryabinin
 */
#include <string>
#include <map>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#ifndef _INTERFACE_H
#define _INTERFACE_H

namespace InterfaceLib {

class Interface {
 public:
    Interface(struct nlmsghdr* h);
    std::string name() const;
    std::string type() const;
    std::string address() const;
    bool operator == (const Interface& m) const;
    bool operator != (const Interface& m) const;
    void print(std::ostream& os) const;
 private:
    std::string _name;
    std::string _type;
    std::string _address;
};

std::ostream& operator<<(std::ostream& os, const Interface& iface);


}

#endif
