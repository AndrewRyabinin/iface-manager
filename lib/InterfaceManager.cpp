/*
 * Copyright (C)  2010 by Jean Lorchat ( http://www.iijlab.net/~jean/iflist.c )
 * Copyright (C)  2011 by Oleg Kutkov  (http://habrahabr.ru/post/121254/)
 * Copyright (C)  2013 by Andrew Ryabinin
 */

#include "InterfaceManager.h"
#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <thread>
#include <sys/types.h>
#include <unistd.h>

namespace InterfaceLib {
using std::shared_ptr;
using std::list;

    InterfaceManager::InterfaceManager():_local({}),_stopThread(false) {
    _fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    if (_fd < 0) {
        throw InterfaceManagerException("Couldn't create netlink socket");
    }

    /* Set 1 sec timeout for recvmsg */
    struct timeval tv;
    tv.tv_sec = 1;
    setsockopt(_fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(struct timeval));

    _local.nl_family = AF_NETLINK;
    _local.nl_pid = getpid();
    _local.nl_groups = RTMGRP_LINK;

    if (bind(_fd, reinterpret_cast<struct sockaddr*>(&_local), sizeof(_local)) < 0) {
        close(_fd);
        throw InterfaceManagerException("Couldn't bind socket");
    }

    fillIfaceList();
}

void InterfaceManager::startListening() {
    _ifaceThread = std::thread(&InterfaceManager::listenIfaceEvents, this);
}

void InterfaceManager::stopListening() {
    _stopThread = true;
}

void InterfaceManager::fillIfaceList() {
    struct iovec io;
    char reply[MSG_BUF_SIZE];

    struct sockaddr_nl	_kernel = {};
    _kernel.nl_family = AF_NETLINK;

    struct nl_req_s {
        struct nlmsghdr hdr;
        struct rtgenmsg gen;
    } req = {};

    req.hdr.nlmsg_len = NLMSG_LENGTH(sizeof(struct rtgenmsg));
    req.hdr.nlmsg_type = RTM_GETLINK;
    req.hdr.nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP; 
    req.hdr.nlmsg_seq = 1;
    req.hdr.nlmsg_pid = getpid();
    req.gen.rtgen_family = AF_PACKET;

    io.iov_base = &req;
    io.iov_len = req.hdr.nlmsg_len;

    struct msghdr rtnl_msg = {};
    rtnl_msg.msg_iov = &io;
    rtnl_msg.msg_iovlen = 1;
    rtnl_msg.msg_name = &_kernel;
    rtnl_msg.msg_namelen = sizeof(_kernel);

    sendmsg(_fd, (struct msghdr *) &rtnl_msg, 0);

    int end = 0;
    while (!end) {
        struct iovec io_reply = {};

        io.iov_base = reply;
        io.iov_len = MSG_BUF_SIZE;

        struct msghdr rtnl_reply = {};
        rtnl_reply.msg_iov = &io;
        rtnl_reply.msg_iovlen = 1;
        rtnl_reply.msg_name = &_kernel;
        rtnl_reply.msg_namelen = sizeof(_kernel);
        
        ssize_t len = recvmsg(_fd, &rtnl_reply, 0);
        if (len) {
            for (struct nlmsghdr *msg_ptr = reinterpret_cast<struct nlmsghdr*>(reply);
                 NLMSG_OK(msg_ptr, len);
                 msg_ptr = NLMSG_NEXT(msg_ptr, len)) {

                switch(msg_ptr->nlmsg_type) {
                case NLMSG_DONE:
                    end++;
                    break;
                case RTM_NEWLINK:
                    {
                        shared_ptr<Interface> iface_ptr(new Interface(msg_ptr));
                        std::lock_guard<std::mutex> lock(_ifaceMutex);
                        _interfaces.push_back(iface_ptr);
                        break;
                    }
                }
            }
        }
    }
}

InterfaceManager::~InterfaceManager() {
     stopListening();
    _ifaceThread.join();
     close(_fd);
}

list<shared_ptr<Interface>> InterfaceManager::interfaces() const {
    std::lock_guard<std::mutex> lock(_ifaceMutex);
    return _interfaces;
}

void InterfaceManager::setNewIfaceCallback(const std::function<void (shared_ptr<Interface>&)>& func) {
    _newIfaceCallback = func;
}

void InterfaceManager::setDelIfaceCallback(const std::function<void (shared_ptr<Interface>&)>& func) {
    _delIfaceCallback = func;
}

void InterfaceManager::listenIfaceEvents() {
    char reply[MSG_BUF_SIZE];
    struct iovec iov = {};
    iov.iov_base = reply;
    iov.iov_len = sizeof(reply);

    struct msghdr msg = {};
    msg.msg_name = &_local;
    msg.msg_namelen = sizeof(_local);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;	

    while (1) {
        if (_stopThread) {
            return ;
        }

        ssize_t len = recvmsg(_fd, &msg, 0);
        if (len) {
            for (struct nlmsghdr *msg_ptr = reinterpret_cast<struct nlmsghdr*>(reply);
                 NLMSG_OK(msg_ptr, len);
                 msg_ptr = NLMSG_NEXT(msg_ptr, len)) {

                if ((msg_ptr->nlmsg_type == RTM_NEWLINK) || (msg_ptr->nlmsg_type == RTM_DELLINK)) {
                    Interface iface(msg_ptr);
                    shared_ptr<Interface> iface_ptr(new Interface(msg_ptr));
                    if (msg_ptr->nlmsg_type == RTM_NEWLINK) {
                        {
                            std::lock_guard<std::mutex> lock(_ifaceMutex);
                            _interfaces.push_back(iface_ptr);
                        }

                        if (_newIfaceCallback) {
                            _newIfaceCallback(iface_ptr);
                        }
                    } else {
                        {
                            std::lock_guard<std::mutex> lock(_ifaceMutex);
                            _interfaces.remove_if(shared_ptr_finder<Interface>(iface_ptr));
                        }

                        if (_delIfaceCallback) {
                            _delIfaceCallback(iface_ptr);
                        }
                    }
                }
            }
        }
     }
}
}
